<?php 
    $detail->galeria = !empty($detail->galeria)?explode(',',$detail->galeria):array();
    foreach($detail->galeria as $n=>$d){
        if(!empty($d)){
            $detail->galeria[$n] = base_url('img/blog/'.$d);
        }
    }
	$page = empty($page)?$this->load->view($_SESSION['lang'].'/blog-detalle',array('detail'=>$detail),TRUE,'paginas'):$page;
    $page = $this->querys->fillFields($page,array('categorias'=>$categorias,'recientes'=>$relacionados));
	foreach($detail as $n=>$d){
        if(is_string($d)){
            $page = str_replace('['.$n.']',$d,$page);
        }
    }
    $page = str_replace('[comentarios]',$comentarios->num_rows(),$page);
    $page = str_replace('[vistas]',0,$page);
    $page = str_replace('[aside]',$this->load->view('frontend/aside',array('categorias'=>$categorias,'recientes'=>$relacionados,'tags'=>explode(',',$detail->tags)),TRUE),$page);
    $page = str_replace('[prev]',$prev,$page);
    $page = str_replace('[next]',$next,$page);    
    $page = $this->load->view('read',array('page'=>$page),TRUE,'paginas');
    
    echo $page;
?>