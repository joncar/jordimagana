<?php 
    require_once APPPATH.'controllers/Panel.php';    
    class Admin extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        public function blog(){
            $crud = $this->crud_function('','');
            //$crud->field_type('foto','image',array('path'=>'img/blog','width'=>'960px','height'=>'637px'));
            $crud->set_field_upload('foto','img/blog');
            $crud->set_field_upload('foto2','img/blog');
            $crud->set_field_upload('foto3','img/blog');
            $crud->set_relation_dependency('blog_subcategorias_id','blog_categorias_id','blog_categorias_id');
            $crud->field_type('tags','tags');
            $crud->field_type('status','true_false',array('0'=>'Borrador','1'=>'Publicado'));
            $crud->field_type('idioma','dropdown',array('ca'=>'Catalán','es'=>'Castellano','en'=>'Ingles'));
            $crud->field_type('blog_subcategorias_id','hidden');
            $crud->field_type('galeria','gallery',array('path'=>'img/blog','width'=>'800','height'=>'464'));
            $crud->columns("blog_categorias_id","foto","titulo","tags","fecha","idioma");
            $crud->add_action('<i class="fa fa-clipboard"></i> Clonar','',base_url('blog/admin/clonarEntrada').'/');
            $crud->field_type('user','string',$this->user->nombre);
            $crud->where('blog_categorias_id',4);
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        public function blog_categorias(){
            $crud = $this->crud_function('',''); 
            $crud->set_subject('Sub Categoria');
            $crud->display_as('blog_categorias_nombre','Nombre');
            $crud->field_type('banner','image',array('path'=>'img/blog','width'=>'1700px','height'=>'500px'));
            $crud->field_type('idioma','dropdown',array('ca'=>'Catalán','es'=>'Castellano','en'=>'Ingles'));
            $crud = $crud->render();
            $crud->title = 'Categorias';
            $this->loadView($crud);
        }

        public function blog_subcategorias(){
            $crud = $this->crud_function('',''); 
            $crud->set_subject('SubCategoria');            
            $crud->field_type('banner','image',array('path'=>'img/blog','width'=>'1700px','height'=>'500px'));
            $crud->field_type('idioma','dropdown',array('ca'=>'Catalán','es'=>'Castellano','en'=>'Ingles'));
            $crud = $crud->render();
            $crud->title = 'SubCategoria';
            $this->loadView($crud);
        }
        
        public function clonarEntrada($id){
            if(is_numeric($id)){
                $entry = new Bdsource();
                $entry->where('id',$id);
                $entry->init('blog',TRUE,'entrada');
                $data = $this->entrada;
                $entry->save($data,null,TRUE);
                header("Location:".base_url('blog/admin/blog/edit/'.$entry->getid()));
            }
        }

        public function portafolio(){
            $this->as['portafolio'] = 'blog';
            $crud = $this->crud_function('','');
            $crud->field_type('foto','image',array('path'=>'img/blog','width'=>'1166px','height'=>'702px'));            
            $crud->field_type('foto2','image',array('path'=>'img/blog','width'=>'570px','height'=>'448px'));            
            $crud->field_type('foto3','image',array('path'=>'img/blog','width'=>'570px','height'=>'448px'));
            $crud->field_type('galeria','hidden');
            $crud->field_type('blog_categorias_id','hidden',3);
            $crud->set_relation('blog_subcategorias_id','blog_subcategorias','blog_subcategorias_nombre',array('blog_categorias_id'=>3)); 
            $crud->field_type('tags','tags');
            $crud->field_type('status','true_false',array('0'=>'Borrador','1'=>'Publicado'));
            $crud->field_type('idioma','dropdown',array('ca'=>'Catalán','es'=>'Castellano','en'=>'Ingles'));
            $crud->field_type('user','string',$this->user->nombre)
                 ->display_as('tags','Info')
                 ->where('blog.blog_categorias_id',3);
            $crud = $crud->render();
            $this->loadView($crud);
        }

        function serveis(){
            $this->as['serveis'] = 'blog';
            $crud = $this->crud_function('','');
            $crud->where('blog.blog_categorias_id',2)
                 ->field_type('blog_categorias_id','hidden',2)
                 ->set_subject('Serveis');
            //$crud->field_type('foto','image',array('path'=>'img/blog','width'=>'960px','height'=>'637px'));
            $crud->field_type('foto','image',array('path'=>'img/blog','width'=>'1170px','height'=>'122px'));
            $crud->field_type('foto2','image',array('path'=>'img/blog','width'=>'832px','height'=>'400px'));
            $crud->field_type('foto3','image',array('path'=>'img/blog','width'=>'832px','height'=>'400px'));
            $crud->field_type('galeria','hidden');
            $crud->set_field_upload('foto4','img/blog');            
            
            $crud->set_relation('blog_subcategorias_id','blog_subcategorias','blog_subcategorias_nombre',array('blog_categorias_id'=>2));
            $crud->field_type('tags','tags');
            $crud->field_type('status','true_false',array('0'=>'Borrador','1'=>'Publicado'));
            $crud->field_type('idioma','dropdown',array('ca'=>'Catalán','es'=>'Castellano','en'=>'Ingles'));
            $crud->columns("blog_subcategorias_id","foto","titulo","tags","fecha","idioma");
            $crud->add_action('<i class="fa fa-clipboard"></i> Clonar','',base_url('blog/admin/clonarEntrada').'/');
            $crud->field_type('user','string',$this->user->nombre);
            $crud = $crud->render();
            $this->loadView($crud);
        }
    }
?>
