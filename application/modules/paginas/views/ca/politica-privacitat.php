[menu]
<!--Page Title-->
<section class="page-title">
	<div class="icon-one"></div>
	<div class="icon-two"></div>
	<div class="auto-container">
		<div class="icon-three"></div>
		<div class="icon-six"></div>
		<div class="icon-five"></div>
		<div class="icon-four"></div>
		<div class="icon-ten"></div>
		<h2>Política de Privac<span class="white-color">itat</span> <span class="blue-circle"></span></h2>
	</div>
</section>
<!--End Page Title-->

<!--About Section-->
<section class="about-section" style="margin-bottom:40px">
	<div class="auto-container">
		<h3>Qui és el responsable del tractament de les seves dades personals?</h3>
		<div class="block-text">JORDI MAGAÑA, amb domicili a l'avinguda Barcelona, 30 d'Igualada, amb N.I.F. nº 46586082L, és la responsable del tractament de les dades personals que vostè ens proporciona i es responsabilitza de les referides dades personals d'acord amb la normativa aplicable en matèria de protecció de dades.</div>		
	<p></p>
	<h3>On emmagatzemem les seves dades?</h3>
		<div class="block-text">Les dades que recopilem sobre vostè s'emmagatzemen dins de l'Espai Econòmic Europeu («EEE»). Qualsevol transferència de les seves dades personals serà realitzada de conformitat amb les lleis aplicables.</div>		
	<p></p>
	<h3>A qui comuniquem les seves dades?</h3>
		<div class="block-text">Les seves dades poden ser compartides per JORDI MAGAÑA. Mai passem, venem ni intercanviem les seves dades personals amb terceres parts. Les dades que s'enviïn a tercers, s'utilitzaran únicament per oferir-li a vostè els nostres serveis. Li detallarem les categories de tercers que accedeixen a les seves dades per a cada activitat de tractament específica.</div>		
	<p></p>
	<h3>Quina és la base legal per al tractament de les seves dades personals?</h3>
		<div class="block-text">En cada tractament específic de dades personals recopilades sobre vostè, li informarem si la comunicació de dades personals és un requisit legal o contractual, o un requisit necessari per subscriure un contracte, i si està obligat a facilitar les dades personals, així com de les possibles conseqüències de no facilitar tals dades.</div>		
	<p></p>
	<h3>Quins són els seus drets?</h3>
		<div class="block-text">Dret d’accés: Qualsevol persona té dret a obtenir confirmació sobre si a JORDI MAGAÑA estem tractant dades personals que els concerneixin, o no. Pot contactar a JORDI MAGAÑA que li remetrà les dades personals que tractem sobre vostè per correu electrònic.</div>		
<p></p>
<div class="block-text">Dret de portabilitat: Sempre que JORDI MAGAÑA processi les seves dades personals a través de mitjans automatitzats sobre la base del seu consentiment o a un acord, vostè té dret a obtenir una còpia de les seves dades en un format estructurat, d'ús comú i lectura mecànica transferida al seu nom o a un tercer. En ella s'inclouran únicament les dades personals que vostè ens hagi facilitat.</div>
	<p></p>
<div class="block-text">Dret de rectificació: Vostè té dret a sol·licitar la rectificació de les seves dades personals si són inexactes, incloent el dret a completar dades que figurin incomplets.</div>
	<p></p>
<div class="block-text">Dret de supressió: <br>Vostè té dret a obtenir sense dilació indeguda la supressió de qualsevol dada personal seva tractada per JORDI MAGAÑA a qualsevol moment, excepte en les següents situacions: 
<br>* té un deute pendent amb JORDI MAGAÑA, independentment del mètode de pagament 
<br>* se sospita o està confirmat que ha utilitzat incorrectament els nostres serveis en els últims quatre anys 
<br>* ha contractat algun servei pel que conservarem les seves dades personals en relació amb la transacció per normativa comptable.</div>
	<p></p>
<div class="block-text">Dret d’oposició al tractament de dades en base a l’interés legítim: Vostè té dret a oposar-se al tractament de les seves dades personals sobre la base de l'interès legítim de JORDI MAGAÑA. En aquest cas, JORDI MAGAÑA no seguirà tractant les dades personals tret que puguem acreditar motius legítims imperiosos per al tractament que prevalguin sobre els seus interessos, drets i llibertats, o bé per a la formulació, l'exercici o la defensa de reclamacions.</div>
<p></p>
<div class="block-text">Dret d’oposició al marketing directe: Vostè té dret a oposar-se al màrqueting directe, incloent l'elaboració de perfils realitzada per a aquest màrqueting directe. Pot desvincular-se del màrqueting directe en qualsevol moment de les següents maneres: *seguint les indicacions facilitades a cada correu de màrqueting</div>
<p></p>
<div class="block-text">Dret a presentar una reclamació davant d’una autoritat de control: Si vostè considera que JORDI MAGAÑA tracta les seves dades d'una manera incorrecta, pot contactar amb nosaltres. També té dret a presentar una queixa davant l'autoritat de protecció de dades competent.</div>
<p></p>
<div class="block-text">Dret a limitació en el tractament: Vostè té dret a sol·licitar que JORDI MAGAÑA limiti el tractament de les seves dades personals en les següents circumstàncies: 
<br>* si vostè s'oposa al tractament de les seves dades sobre la base de l'interès legítim de JORDI MAGAÑA. En aquest cas JORDI MAGAÑA haurà de limitar qualsevol tractament d'aquestes dades a l'espera de la verificació de l'interès legítim. 
<br>* si vostè reclama que les seves dades personals són incorrectes, JORDI MAGAÑA ha de limitar qualsevol tractament d'aquestes dades fins que es verifiqui la precisió dels mateixos. 
<br>* si el tractament és il·legal, vostè podrà oposar-se al fet que s'eliminin les dades personals i, en el seu lloc, sol·licitar la limitació del seu ús. 
<br>* si JORDI MAGAÑA ja no necessita les dades personals, però vostè els necessita per a la formulació, l'exercici o la defensa de reclamacions.</div>
<p></p>
<div class="block-text">Exercici de drets: Ens prenem molt de debò la protecció de dades i, per tant, comptem amb personal de servei al client dedicat que s'ocupa de les seves sol·licituds en relació amb els drets abans esmentats. Sempre pot posar-se en contacte amb ells a info@jordimagana.com</div>		
	<p></p><p></p>
	
	
	
	</div>
	</section>
	
[footer]