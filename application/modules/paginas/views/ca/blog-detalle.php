[menu]
    <!--Page Title-->
    <section class="page-title">
    	<div class="icon-one"></div>
        <div class="icon-two"></div>

    	<div class="auto-container">
        	<div class="icon-three"></div>
            <div class="icon-six"></div>
			<div class="icon-four"></div>
            <div class="icon-five"></div>
        	<h2><span class="blue-triangle-two"></span> [titulo]<span class="pink-circle"></span></h2>
        </div>
    </section>
    <!--End Page Title-->
    
    <!--Sidebar Page Container-->
    <div class="sidebar-page-container">
    	<div class="auto-container">
        	<div class="row clearfix">
            	
                <!--Content Side-->
                <div class="content-side col-lg-12 col-md-12 col-sm-12 col-xs-12">
                	<div class="blog-sidebar">
						
                        <!--News Block Two-->
                        <div class="news-single">
                        	<div class="inner-box">
                            	<!-- Preambule -->
                                <div class="image template-post-section-preambule">
                                    <!-- Image -->
                                    <img src="<?= !empty($detail->galeria)?$detail->galeria[0]:$detail->foto  ?>" alt="" />
                                </div>
                                <div class="galleryBlog template-component-image template-fancybox">
                                    <?php foreach($detail->galeria as $f): ?>
                                        <a href="<?= $f ?>" class="">
                                            <img src="<?= $f ?>" alt="">
                                        </a>
                                    <?php endforeach ?>
                                </div>
                                <div class="lower-content">
                                	<h2>[titulo]</h2>
                                    <ul class="post-meta">
                                        <li>[fecha]</li>                                        
                                    </ul>
                                    <div class="text">
                                    	[texto]                                    	
                                    </div>
                                    <div class="tags">
                                    	[tags]
                                    </div>
                                    <!--post-share-options-->
                                    <div class="post-share-options clearfix">
                                        <div class="pull-left">
                                        	<div class="pull-left"><span class="share">Comparte esta noticia</span></div>
                                        </div>
                                        <div class="pull-right">
                                        	<ul class="social-icon-four">
                                                <li class="facebook"><a href="#"><span class="fa fa-facebook-f"></span></a></li>
                                                <li class="twitter"><a href="#"><span class="fa fa-twitter"></span></a></li>
                                                <li class="linkedin"><a href="#"><span class="fa fa-google-plus"></span></a></li>
                                                <li class="vimeo"><a href="#"><span class="fa fa-pinterest-p"></span></a></li>
                                                <li class="vimeo"><a href="#"><span class="fa fa-dribbble"></span></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            
                            
                        </div>
                        
                    </div>
                </div>
                
                <!--Sidebar Side-->
                
                
            </div>
        </div>
    </div>
[footer]

