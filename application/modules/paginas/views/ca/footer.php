<!--Main Footer-->
<footer class="main-footer" style="background-image:url([base_url]theme/theme/images/background/3.jpg)">
	<div class="auto-container">
		<div class="logo-box">
			<div class="logo">
				<a href="#"><img src="[base_url]theme/theme/images/footer-logo.png" alt="" /></a>
			</div>
		</div>
		<!--Widgets Section-->
		<div class="widgets-section">
			<div class="row clearfix">
				
				<!--Footer Column-->
				<div class="column col-md-3 col-sm-12 col-xs-12">
					<ul class="info-list">
						<li>info@jordimagaña.com</li>
						<li>(0034) 93 805 37 38</li>
					</ul>
				</div>
				
				<!--Footer Column-->
				<div class="column col-md-6 col-sm-12 col-xs-12">
					<div class="copyright">&copy; Copyright 2018 Jordi Magaña. Tots els dret reservats </div>
				</div>
				
				<!--Footer Column-->
				<div class="column col-md-3 col-sm-12 col-xs-12">
					<div class="location">Avda. Barcelona 30 Igualada Barcelona. CAT</div>
					<ul class="social-icon-two">
						<li><a href="#"><span class="fa fa-facebook"></span></a></li>
						<li><a href="#"><span class="fa fa-twitter"></span></a></li>
						<li><a href="#"><span class="fa fa-google-plus"></span></a></li>
						<li><a href="#"><span class="fa fa-pinterest"></span></a></li>
						<li><a href="#"><span class="fa fa-dribbble"></span></a></li>
					</ul>
				</div>
				
			</div>
		</div>
	</div>
</footer>
<!--End Main Footer-->