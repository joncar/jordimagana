[menu]
<!--Page Title-->
    <section class="page-title">
    	<div class="icon-one"></div>
        <div class="icon-two"></div>
		<div class="auto-container">
        	<div class="icon-three"></div>
            <div class="icon-six"></div>
			<div class="icon-five"></div>
			<div class="icon-four"></div>
			<div class="icon-ten"></div>
        	<h2><?= $detail->titulo ?> <span class="yellow-circle"></span></h2>
        </div>
    </section>
    <!--End Page Title-->

    
    <!--Gallery Single Section-->
    <section class="gallery-single-section">
    	<div class="auto-container">
        	<div class="portfolio-gallery">
            	<div class="row clearfix">
                	
                	<div class="column col-md-12 col-sm-12 col-xs-12">
                    	<div class="image wow fadeInDown" data-wow-delay="0ms" data-wow-duration="1500ms">
                        	<a href="<?= $detail->foto ?>" data-fancybox="images" data-caption="" ><img src="<?= $detail->foto ?>" alt="" /></a>
                        </div>
                    </div>
                    <div class="column col-md-6 col-sm-6 col-xs-12">
                    	<div class="image wow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
                        	<a href="<?= $detail->foto2 ?>" data-fancybox="images" data-caption="" ><img src="<?= $detail->foto2 ?>" alt="" /></a>
                        </div>
                    </div>
                    <div class="column col-md-6 col-sm-6 col-xs-12">
                    	<div class="image wow fadeInRight" data-wow-delay="0ms" data-wow-duration="1500ms">
                        	<a href="<?= $detail->foto3 ?>" data-fancybox="images" data-caption="" ><img src="<?= $detail->foto3 ?>" alt="" /></a>
                        </div>
                    </div>
                </div>
            </div>

            <h2><?= $detail->titulo ?></h2>
            <div class="row clearfix">
            
            	<!--Content Column-->
                <div class="content-column col-md-8 col-sm-12 col-xs-12">
                	<div class="inner-column">
                    	<div class="text">
                        	<?= $detail->texto ?>
                        </div>
                    </div>
                </div>
                
                <!--Info Column-->
                <div class="info-column col-md-4 col-sm-12 col-xs-12">
                	<div class="inner-column">
                    	<h3>Info</h3>
                        <ul class="portfolio-info-list">
                        	<?php foreach(explode(',',$detail->tags) as $t): ?>
                        		<?php $tag = explode(':',$t,2); ?>
                        		<li><span><?= trim(@ucfirst($tag[0])) ?>:</span><?= trim(@ucfirst($tag[1])) ?></li>                        	
                        	<?php endforeach ?>
                        </ul>
                        <h3>Share</h3>
                        <ul class="social-icon-two">
                            <li><a href="#"><span class="fa fa-facebook"></span></a></li>
                            <li><a href="#"><span class="fa fa-twitter"></span></a></li>
                            <li><a href="#"><span class="fa fa-google-plus"></span></a></li>
                            <li><a href="#"><span class="fa fa-pinterest"></span></a></li>
                            <li><a href="#"><span class="fa fa-dribbble"></span></a></li>
                        </ul>
                    </div>
                </div>
                
            </div>
            
            <!--News Posts-->
            <div class="new-posts">
                <div class="clearfix">
                    <div class="pull-left">
                        [prev]
                    </div>
                    <a href="#" class="grid-btn"><span class="flaticon-grid-2"></span></a>
                    <div class="pull-right">
                        [next]
                    </div>
                </div>
            </div>
            <!--End News Posts-->
            
        </div>
    </section>
    <!--End Gallery Single Section-->
[footer]