[menu]
<!--Page Title-->
    <section class="page-title">
    	<div class="icon-one"></div>
        <div class="icon-two"></div>
		<div class="auto-container">
        	<div class="icon-three"></div>
            <div class="icon-six"></div>
			<div class="icon-five"></div>
			<div class="icon-four"></div>
			<div class="icon-thirteen"></div>
        	<h2><span class="icon-twelve"></span> <span class="white-color">Port</span>foli</h2>
        </div>
    </section>
    <!--End Page Title-->

    
    <!--Gallery Section Three-->
    <section class="gallery-section-three style-two">
    	<div class="auto-container">
        	
            <!--MixitUp Galery-->
            <div class="mixitup-gallery">
                
                <!--Filter-->
                <div class="filters clearfix">
                    
                    <ul class="filter-tabs text-center filter-btns clearfix">
                        <li class="active filter" data-role="button" data-filter="all">Tots</li>
                        <?php foreach($this->db->get_where('blog_subcategorias',array('blog_categorias_id'=>3))->result() as $p): ?>
                            <li class="filter" data-role="button" data-filter=".port<?= $p->id ?>"><?= $p->blog_subcategorias_nombre ?></li>
                        <?php endforeach ?>
                    </ul>
                    
                </div>
                
                <div class="filter-list row clearfix">
                
                    <?php foreach($this->db->get_where('blog',array('blog_categorias_id'=>3))->result() as $p): ?>
                        <!--Gallery Block-->
                        <div class="gallery-item mix all port<?= $p->blog_subcategorias_id ?> col-md-4 col-sm-6 col-xs-12">
    						<div class="inner-box wow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
                                <div class="image">
                                    <img src="<?= base_url('img/blog/'.$p->foto) ?>" alt="<?= $p->titulo ?>" />
                                    <div class="overlay-box">
                                        <div class="overlay-inner">
                                            <div class="content">
                                                <?php if(strpos($p->texto,'http://') || strpos($p->texto,'https://')): ?>
                                                <a href="<?= strip_tags($p->texto) ?>" title="<?= $p->titulo ?>" class="plus"><span class="flaticon-signs"></span></a>
                                            <?php elseif(strpos($p->texto,'www')): ?>
                                                <a href="http://<?= strip_tags($p->texto) ?>" title="<?= $p->titulo ?>" class="plus"><span class="flaticon-signs"></span></a>
                                            <?php else: ?>
                                                <div class="lightbox">
                                                    <a href="<?= base_url('img/blog/'.$p->foto) ?>" data-src="<?= base_url('img/blog/'.$p->foto) ?>" title="<?= $p->titulo ?>" class="plus"><span class="flaticon-signs"></span></a>
                                                </div>
                                            <?php endif ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach ?>
                    
                    
                </div>
            </div>
            
        </div>
    </section>
    <!--End Gallery Section Three-->
[footer]