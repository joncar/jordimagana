<!-- Main Header-->
    <header class="main-header">
        <!--Header Spacing-->
        <div class="header-spacing"></div>
        
        <!--Header-Upper-->
        <div class="main-box">
            <div class="auto-container">
                <div class="outer-container clearfix">
                    <div class="pull-left logo-outer">
                        <div class="logo"><a href="<?= base_url() ?>"><img src="[base_url]theme/theme/images/logo.png" alt="" title=""></a></div>
                    </div>
                
                    <div class="pull-right upper-right clearfix">
                        
                        <div class="nav-outer clearfix">
                            
                            <!-- Main Menu -->
                            <nav class="main-menu">
                                <!--<div class="navbar-header">
                                    <!-- Toggle Button -->      
                                    <!--<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    </button>
                                </div>-->
                                
                                <div class="navbar-collapse collapse clearfix">
                                    <ul class="navigation clearfix">
                                        <li class="current">
                                            <a href="[base_url]">Inici</a>
                                        </li>
                                        <li>
                                            <a href="[base_url]nosaltres.html">Nosaltres</a>
                                        </li>
                                        <li class="dropdown">
                                            <a href="[base_url]serveis.html">Serveis <i class="fa fa-chevron-down" style="font-size:12px;"></i></a>
                                            <ul>
                                                <?php foreach($this->db->get_where('blog_subcategorias',array('blog_categorias_id'=>2))->result() as $ca): ?>
                                                    <li style="color:<?= $ca->color_texto ?>"><b><?= mb_strtoupper($ca->blog_subcategorias_nombre) ?></b></li>
                                                    <?php foreach($this->db->get_where('blog',array('blog_subcategorias_id'=>$ca->id))->result() as $b): ?>
                                                        <li><a href="<?= base_url('serveis.html') ?>#serveis<?= $b->id ?>"><?= mb_strtoupper($b->titulo) ?></a></li>
                                                    <?php endforeach ?>
                                                <?php endforeach ?>
                                            </ul>
                                        </li>
                                        <li>
                                            <a href="[base_url]portafolio.html">Portafoli</a>
                                        </li>                                        
                                        <li>
                                            <a href="[base_url]blog">Blog</a>                                           
                                        </li>
                                        <li><a href="[base_url]contacta.html">Contacte</a></li>
                                    </ul>
                                </div>
                            </nav>
                            
                            <!-- Hidden Nav Toggler -->
                            <div class="outer-box nav-toggler">
                               <div class="nav-btn">
                                    <button class="hidden-bar-opener">
                                        <span class="icon flaticon-menu"></span>
                                    </button>
                                </div>
                            </div>
                            <!-- / Hidden Nav Toggler -->
                            
                        </div>
                        
                    </div>
                </div>    
            </div>
        </div>
        <!--End Header Upper-->
        

        <!--FullScreen Menu-->
        <div class="fullscreen-menu">
            <!--Close Btn-->
            <div class="close-menu"><span class="flaticon-cancel-1"></span></div>
            
            <div class="menu-outer-container">
                <div class="menu-box">
                    <nav class="full-menu">
                        <ul class="navigation">
                            <li class="current"><a href="[base_url]">Inici</a></li>
                            <li>
                                <a href="[base_url]nosaltres.html">Nosaltres</a>
                            </li>
                            <li>
                                <a href="[base_url]serveis.html">Serveis</a>
                            </li>
                            <li>
                                <a href="[base_url]portafolio.html">Portfolio</a>
                            </li>                                        
                            <li>
                                <a href="[base_url]blog">Blog</a>                                           
                            </li>
                            <li><a href="[base_url]contacta.html">Contacte</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div><!--End FullScreen Menu-->
    </header>
    <!--End Main Header -->
    <!-- Hidden Navigation Bar -->
    <section class="hidden-bar right-align">
        
        <div class="hidden-bar-closer">
            <button><span class="flaticon-cancel-1"></span></button>
        </div>
        
        <!-- Hidden Bar Wrapper -->
        <div class="hidden-bar-wrapper">
            
            <div class="logo">
                <a href="index.html"><img src="[base_url]theme/theme/images/logo-2.png" alt="" /></a>
            </div>
            <div class="content-box">
                <h2>Nosaltres</h2>
                <div class="text">Som un equip multidisciplinari i trilingüe en els sectors del disseny gràfic, la comunicació i el desenvolupament de webs i apps. Comptem amb més de 30 anys d’experiència desenvolupant projectes d'èxit. Disposem d’una gran xarxa de col·laboradors especialitzats en àmbits molt concrets del camp de la comunicació que ens permeten garantir qualitat i professionalitat.</div>
                <div class="contact-form" style="margin:20px 0">
                    <a href="<?= base_url() ?>contacta.html#formulario">
                        <button class="theme-btn btn-style-one" type="button">Contacta'ns</button>
                    </a>
                </div>                
            </div>
            <div class="contact-info">
                <h2>Info de contacte</h2>
                <ul class="list-style-one">
                    <li><span class="icon fa fa-map-marker"></span>Avda Barcelona 30. <br>08700 Igualada /BCN/</li>
                    <li><span class="icon fa fa-phone"></span>+34 000 57 23</li>
                    <li><span class="icon fa fa-envelope-o"></span>info@jordimagana.com</li>
                    <li><span class="icon fa fa-clock-o"></span>Horari: 09.00h a 20.00h </li>
                </ul>
            </div>
        </div><!-- / Hidden Bar Wrapper -->
        
    </section>
    <!-- End / Hidden Bar -->