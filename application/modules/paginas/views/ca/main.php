[menu]
<!--Main Banner Section-->
<section class="main-banner-section">
	<span class="left-curve"></span>
	<span class="top-icon"></span>
	<span class="icon-four"></span>
	<span class="icon-five"></span>
	<div class="auto-container">
		<div class="inner-container" style="background-image:url([base_url]theme/theme/images/background/2.jpg)">
			<div class="circle-one"></div>
			<div class="icon-one"></div>
			<div class="icon-two"></div>
			<div class="icon-three"></div>
			<div class="icon-six"></div>
			<div class="icon-seven"></div>
			<div class="icon-eight"></div>
			<div class="icon-nine"></div>
			<div class="icon-ten"></div>
			<div class="icon-eleven"></div>
			<h2>Som gent creativa</h2>
			<div class="text">Ens agrada imaginar, inventar, crear...<br/> Desenvolupem grans idees per a petites i grans empreses.</div>
			<!--<div class="scroll scroll-to-target" data-target=".about-section">Baixa</div>-->
			<div class='icon-scroll'></div>
			<div class="side-image"><img src="[base_url]theme/theme/images/resource/banner-image.png" alt="" /></div>
			<ul class="social-icon-one">
				<li><a href="#"><span class="fa fa-facebook"></span></a></li>
				<li><a href="#"><span class="fa fa-twitter"></span></a></li>
				<li><a href="#"><span class="fa fa-google-plus"></span></a></li>
				<li><a href="#"><span class="fa fa-pinterest"></span></a></li>
				<li><a href="#"><span class="fa fa-dribbble"></span></a></li>
			</ul>
		</div>
	</div>
</section>
<!--End Main Banner Section-->
<!--About Section-->
<section class="about-section">
	<div class="auto-container">
		<h2>presentació</h2>
		<div class="text">Som un estudi creatiu de disseny gràfic especialitzat en identitat corporativa, disseny editorial, packaging, desenvolupament de webs i apps, així com també també en el àmbit de la comunciació i les xarxes socials. Desenvolupem projectes a  mida segons les necessitats de cada un dels nostres clients amb el disseny com el principal valor de tots els nostres treballs. </div>
		<div class="row clearfix">
			<!--About Block-->
			<div class="about-block col-md-4 col-sm-6 col-xs-12">
				<div class="inner-box">
					<div class="icon-box">
						<img src="http://www.jordimagana.com/theme/theme/images/1.svg" style=" width: 18%;" alt="">
					</div>
					<h3><a href="<?= base_url('nosaltres.html') ?>">Objectiu</a></h3>
					<div class="block-text">El nostre objectiu és aconseguir que tu aconsegueixi el teu objectiu. Busquem l'èxit dels teus projectes a través de la conciliació entre l’estètica i la funcionalitat. </div>
				</div>
			</div>
			<!--About Block-->
			<div class="about-block col-md-4 col-sm-6 col-xs-12">
				<div class="inner-box">
					<div class="icon-box">
						<img src="http://www.jordimagana.com/theme/theme/images/2.svg" style=" width: 18%;" alt="">
					</div>
					<h3><a href="<?= base_url('nosaltres.html') ?>">Dedicació</a></h3>
					<div class="block-text">Cuidem els nostres treballs durant tot el procés creatiu i posada en funcionament. Oferim dedicació i continuïtat. Vetllem per la qualitat de cada un dels nostres projectes. </div>
				</div>
			</div>
			<!--About Block-->
			<div class="about-block col-md-4 col-sm-6 col-xs-12">
				<div class="inner-box">
					<div class="icon-box">
						<img src="http://www.jordimagana.com/theme/theme/images/3.svg" style=" width: 18%;" alt="">
					</div>
					<h3><a href="<?= base_url('nosaltres.html') ?>">Valors</a></h3>
					<div class="block-text">La comunicació, el compromís, la serietat i el respecte són els valors que ens identifiquen i els abanderats de la nostra tasca. Gaudim del nostre treball al costat teu, compartint la passió pel disseny gràfic. </div>
				</div>
			</div>
		</div>
	</div>
</section>
<!--End About Section-->
<!--Portfolio Section-->
<section class="portfolio-section">
	<div class="icon-two"></div>
	<div class="icon-three"></div>
	<div class="auto-container">
		<div class="icon-one"></div>
		<div class="sec-title centered">
			<div class="title">Una mostra dels nostres treballs</div>
			<h2>Portfolio</h2>
		</div>
		<!--Sortable Masonry-->
		<div class="sortable-masonry">
			<!--Filter-->
			<div class="filters clearfix">
				<ul class="filter-tabs filter-btns clearfix">
					<li class="active filter" data-role="button" data-filter=".all">Tots</li>
					<?php foreach($this->db->get_where('blog_subcategorias',array('blog_categorias_id'=>3))->result() as $p): ?>
                        <li class="filter" data-role="button" data-filter=".port<?= $p->id ?>"><?= $p->blog_subcategorias_nombre ?></li>
                    <?php endforeach ?>
				</ul>
			</div>
			<div class="items-container row clearfix">
				
				<?php 	
					$cat = mt_rand(14,15);					
					$nn = 0;
					foreach($this->db->get_where('blog_subcategorias',array('blog_categorias_id'=>3))->result() as $c):
					$this->db->limit(6);
					foreach($this->db->get_where('blog',array('blog_subcategorias_id'=>$c->id))->result() as $n=>$p): ?>
                    <!--Gallery Block-->
                	<?php 
                		$all = ''; 
                		if($p->blog_subcategorias_id==$cat){
                			$all = $nn<12?'all':'';
                			$nn++;
                		}
                	?>
                    <div class="gallery-item masonry-item <?= $all ?> port<?= $p->blog_subcategorias_id ?> col-md-4 col-sm-6 col-xs-6">
						<div class="inner-box wow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
                            <div class="image">
                                <img src="<?= base_url('img/blog/'.$p->foto) ?>" alt="<?= $p->titulo ?>" />
                                <div class="overlay-box">
                                    <div class="overlay-inner">
                                        <div class="content">
                                            
                                            <?php if(strpos($p->texto,'http://') || strpos($p->texto,'https://')): ?>
                                                <a href="<?= strip_tags($p->texto) ?>" title="<?= $p->titulo ?>" class="plus"><span class="flaticon-signs"></span></a>
                                            <?php elseif(strpos($p->texto,'www')): ?>
                                                <a href="http://<?= strip_tags($p->texto) ?>" title="<?= $p->titulo ?>" class="plus"><span class="flaticon-signs"></span></a>
                                            <?php else: ?>
                                            	<div class="lightbox">
                                                	<a href="<?= base_url('img/blog/'.$p->foto) ?>" data-src="<?= base_url('img/blog/'.$p->foto) ?>" title="<?= $p->titulo ?>" class="plus"><span class="flaticon-signs"></span></a>
                                            	</div>
                                            <?php endif ?>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach; endforeach; ?>
				
			</div>
			<div class="btn-box text-center">
				<a href="[base_url]portafolio.html" class="theme-btn btn-style-one">Més projectes <span class="arrow flaticon-arrow-pointing-to-right"></span></a>
			</div>
		</div>
	</div>
</section>
<!--End Portfolio Section-->
<!--Services Section-->
<?php 	
    $serveis = $this->db->get_where('blog',array('blog_categorias_id'=>2));
    $testimonios = $this->db->get_where('blog',array('blog_subcategorias_id'=>13));
?>

<section class="services-section">
	<div class="icon-one"></div>
	<div class="icon-four"></div>
	<div class="auto-container">
		<div class="icon-two"></div>
		<div class="icon-three"></div>
		<div class="icon-five"></div>
		<div class="row clearfix">
			<div class="column col-md-4 col-sm-6 col-xs-12">
				<div class="sec-title">

					<div class="title">Serveis</div>
					<h2><?= $serveis->row()->titulo ?></h2>
				</div>
			</div>

			<div class="column col-md-8 col-sm-12 col-xs-12">
				
				<?php foreach($this->db->get_where('blog_subcategorias',array('blog_categorias_id'=>2))->result() as $ca): ?>
					<div class="row" style="margin-bottom: 70px;">
						<div class="col-xs-12" style="">
							<div class="col-xs-12">
								<h3 style="margin-bottom: 0; border-bottom: 1px solid #e0e0e0"><?= $ca->blog_subcategorias_nombre ?></h3>
							</div>
						</div>
					</div>
					<div class="row clearfix">
						
						<?php foreach($this->db->get_where('blog',array('blog_subcategorias_id'=>$ca->id))->result() as $b): ?>
		                    <!--Services Block Two-->
		                    <div class="services-block col-md-4 col-sm-4 col-xs-4">
		                    	<div class="inner-box">

	                                <div class="about-block col-xs-12 <?= $ca->color ?>" style="margin-bottom: 0;">
	                                    <div class="inner-box">
	                                        <div class="icon-box">
	                                            <!--<span class="icon flaticon-gear"></span>-->
	                                            <img src="<?= base_url('img/blog/'.$b->foto4) ?>" alt="">
	                                        </div>

	                                    </div>
	                                </div>
	                                <div class="col-xs-12">
			                        	<h3 style="margin-bottom: 0"><a href="<?= base_url('serveis.html') ?>#serveis<?= $b->id ?>"><?= $b->titulo ?></a></h3>
			                            <a class="read-more" href="<?= base_url('serveis.html') ?>#serveis<?= $b->id ?>">Llegir més <span class="icon flaticon-arrow-pointing-to-right"></span></a>
		                            </div>
		                        </div>
		                    </div>
		                <?php endforeach ?>

					</div>
				<?php endforeach ?>
			</div>
		</div>
	</div>
</section>
<!--End Services Section-->
<!--Team Section-->
<section class="team-section">
	<div class="icon-three"></div>
	<div class="auto-container">
		<div class="icon-one"></div>
		<div class="icon-two"></div>
		<div class="sec-title centered">
			<div class="title">Equip</div>
			<h2>Professionals creatius</h2>
		</div>
		<div class="row clearfix">
			<!--Team Block-->
			<div class="team-block col-md-3 col-sm-6 col-xs-6">
				<div class="inner-box">
					<div class="image">
						<img src="[base_url]theme/theme/images/resource/team-1.jpg" alt="" />
						<div class="social-box">
							<a href="#"><span class="fa fa-facebook"></span></a>
							<a href="#"><span class="fa fa-twitter"></span></a>
							<a href="#"><span class="fa fa-pinterest-p"></span></a>
						</div>
					</div>
					<div class="lower-box">
						<h3><a href="about.html">Jordi Magaña</a></h3>
						<div class="designation">Art Director</div>
					</div>
				</div>
			</div>
			<!--End Team Block-->
			<!--Team Block-->
			<div class="team-block col-md-3 col-sm-6 col-xs-6">
				<div class="inner-box">
					<div class="image">
						<img src="[base_url]theme/theme/images/resource/team-2.jpg" alt="" />
						<div class="social-box">
							<a href="#"><span class="fa fa-facebook"></span></a>
							<a href="#"><span class="fa fa-twitter"></span></a>
							<a href="#"><span class="fa fa-pinterest-p"></span></a>
						</div>
					</div>
					<div class="lower-box">
						<h3><a href="about.html">Eliana Tusell</a></h3>
						<div class="designation">Dissenyadora</div>
					</div>
				</div>
			</div>
			<!--End Team Block-->
			<!--Team Block-->
			<div class="team-block col-md-3 col-sm-6 col-xs-6">
				<div class="inner-box">
					<div class="image">
						<img src="[base_url]theme/theme/images/resource/team-3.jpg" alt="" />
						<div class="social-box">
							<a href="#"><span class="fa fa-facebook"></span></a>
							<a href="#"><span class="fa fa-twitter"></span></a>
							<a href="#"><span class="fa fa-pinterest-p"></span></a>
						</div>
					</div>
					<div class="lower-box">
						<h3><a href="about.html">Miriam Salazar</a></h3>
						<div class="designation">Comunicació</div>
					</div>
				</div>
			</div>
			<!--End Team Block-->
			<!--Team Block-->
			<div class="team-block col-md-3 col-sm-6 col-xs-6">
				<div class="inner-box">
					<div class="image">
						<img src="[base_url]theme/theme/images/resource/team-4.jpg" alt="" />
						<div class="social-box">
							<a href="#"><span class="fa fa-facebook"></span></a>
							<a href="#"><span class="fa fa-twitter"></span></a>
							<a href="#"><span class="fa fa-pinterest-p"></span></a>
						</div>
					</div>
					<div class="lower-box">
						<h3><a href="about.html">Jonathan Cardozo</a></h3>
						<div class="designation">Desarrollador</div>
					</div>
				</div>
			</div>
			<!--End Team Block-->
		</div>
	</div>
</section>
<!--End Team Section-->
<!--Testimonial Section-->


<!--End Testimonial Section-->
<!--Blog Section-->
<section class="blog-section">
	<div class="auto-container">
		<div class="icon-one"></div>
		
		<div class="row clearfix">
			<div class="column col-md-4 col-sm-12 col-xs-12">
			
				<div class="sec-title">
					<div class="title">Blog</div>
					<h2>De nosaltres, amb amor</h2>
				</div>
			</div>
			<div class="blogs-column col-md-12 col-sm-12 col-xs-12">
				<div class="inner-column">
					
					
					[foreach:blog]
						<div class="row">
							<div class="col-xs-12 col-sm-offset-2 col-sm-2">
								<img src="[foto]">
							</div>
							<!--News Block-->
							<div class="col-xs-12 col-sm-8">
								<div class="news-block">
									<div class="inner-box">
										<h3><a href="[link]">[titulo]</a></h3>
										<ul class="post-meta">
											<li><a href="[link]">[fecha]</a></li>
										</ul>
										<div class="text">[texto]</div>
										<a href="[link]" class="read-more">Veure més <span class="arrow flaticon-arrow-pointing-to-right"></span></a>
									</div>
								</div>
							</div>
						</div>
					[/foreach]
					
					
					<div class="btn-box" style="text-align:center">
						<a href="[base_url]blog" class="theme-btn btn-style-one">Més notícies <span class="arrow flaticon-arrow-pointing-to-right"></span></a>
					</div>
					
				</div>
				
			</div>
		</div>
	</div>
</section>
<!--End Blog Section-->
[footer]
</div>
<!--End pagewrapper-->