[menu]
<?php 
    $serveis = $this->db->get_where('blog',array('blog_categorias_id'=>2));
    $testimonios = $this->db->get_where('blog',array('blog_subcategorias_id'=>13));
?>
 <!--Page Title-->
    <section class="page-title">
    	<div class="icon-one"></div>
        <div class="icon-two"></div>

    	<div class="auto-container">
        	<div class="icon-three"></div>
            <div class="icon-six"></div>
			<div class="icon-four"></div>
            <div class="icon-five"></div>
            <div class="icon-nine"></div>
        	<h2><span class="blue-triangle-two"></span> Serve<span class="white-color">is</span><span class="lined-pink"></span></h2>
        </div>
    </section>
    <!--End Page Title-->

    <section class="services-section-two">
        
        <div class="auto-container">
            <h2><?= $serveis->row()->titulo ?></h2>
            <div class="row clearfix">
            </div>
        </div>

        <?php foreach($this->db->get_where('blog_subcategorias',array('blog_categorias_id'=>2))->result() as $ca): ?>
            <div class="auto-container" style="margin-top:140px; ">
                <div class="row" style="text-align: center">
                    <div class="col-xs-12">
                        <h2 style="border-bottom: 1px solid #e0e0e0"><?= $ca->blog_subcategorias_nombre ?></h2>
                    </div>
                </div>
            </div>

            <?php foreach($this->db->get_where('blog',array('blog_subcategorias_id'=>$ca->id))->result() as $b): ?>
                <div  id="serveis<?= $b->id ?>" style="height:2em;"></div>
                <div class="auto-container">

                    <div class="row" style="margin-top:100px; margin-bottom:70px;">
                        <div class="col-xs-12" style="text-align: center;">
                            <div class="about-block <?= $ca->color ?>" style="display: inline-block !important; margin-bottom: 0">
                                <div class="inner-box" style="">
                                    <div class="icon-box" style="">
                                        <!--<span class="icon flaticon-gear"></span>-->
                                        <img src="<?= base_url('img/blog/'.$b->foto4) ?>" alt="">
                                    </div>                                   
                                </div>
                            </div>
                             <h2 style="text-align:center;">
                                <?= $b->titulo ?>
                            </h2>
                        </div>
                        <div class="col-xs-12" style="text-align:center; margin-bottom:70px;">
                            <p><?= $b->texto ?></p>
                        </div>
                        <div class="col-xs-12" style="margin-bottom: 40px;">
                            <div src="" style="width:100%; height:122px; background:url(<?= base_url('img/blog/'.$b->foto) ?>);"></div>
                        </div>
                    </div>
                </div>

                 <div class="row" style=" margin-bottom:70px; margin-left:0; margin-right: 0">
                    <div class="col-xs-12 col-sm-6" style="padding:0">
                        <div style="background:url(<?= base_url('img/blog/'.$b->foto2) ?>); width:100%; height:400px;background-position: center"></div>
                    </div>
                    <div class="col-xs-12 col-sm-6" style="padding:0">
                        <div style="background:url(<?= base_url('img/blog/'.$b->foto3) ?>); width:100%; height:400px;background-position: center"></div>
                    </div>
                </div>
            <?php endforeach ?>

           
        <?php endforeach ?>
            
            

        
    </section>




    
    
    <!--Testimonial Section-->
    <!-- 
<section class="testimonial-section style-two">
    	<div class="auto-container">
        	<div class="single-item-carousel owl-carousel owl-theme">
            	
                <?php foreach($testimonios->result() as $t): ?>
                    <!~~Testimonial Block~~>
                    <div class="testimonial-block style-two">
                    	<div class="inner-box">
                        	<div class="quote-icon">
                            	<span class="icon flaticon-left-quote-1"></span>
                            </div>
                            <div class="text"><?= strip_tags($t->texto) ?></div>
                            <div class="designation"><?= $t->titulo ?></div>
                        </div>
                    </div>
                <?php endforeach ?>
               
                
            </div>
            
        </div>
    </section>
 -->
    <!--End Testimonial Section-->
[footer]