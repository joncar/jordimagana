[menu]
<!--Page Title-->
    <section class="page-title">
    	<div class="icon-one"></div>
        <div class="icon-two"></div>
    	<div class="auto-container">
        <div class="icon-three"></div>
        <div class="icon-four"></div>
        <div class="icon-five"></div>
        <div class="icon-six"></div>
        <div class="icon-seven"></div>
        	<h2>Contacta<span class="blue-triangle"></span></h2>
        </div>
    </section>
    <!--End Page Title-->
    
    <!--Map Section-->
    <section class="map-section">
    	<!--Map Outer-->
        <div class="map-outer">
            <!--Map Canvas-->
            <div class="map-canvas"
                data-zoom="16"
                data-lat="41.5850149"
                data-lng="1.6168613"
                data-type="roadmap"
                data-hue="#ffc400"
                data-title="Jordi Magaña"
                data-icon-path="[base_url]img/map-marker.png"
                data-content="Av. de Barcelona, 30<br>08700 Igualada Bcn.<br/><a href='mailto:info@jordimagana.com'>info@jordimagana.com</a>">
            </div>
        </div>
    </section>
    <!--End Map Section-->
    
    <!--Contact Section-->
    <section class="contact-section" id="formulario">
    	<div class="auto-container">
        	<div class="row clearfix">
            	
                <!--Info Column-->
                <div class="info-column col-lg-3 col-md-4 col-sm-12 col-xs-12">
                	<div class="inner-column">
                    	<h2>Contacta</h2>
                        <div class="location">Av. de Barcelona, 30<br>08700 Igualada Bcn.</div>
                        <ul class="info-list">
                        	<li><a href="tel:+34930005723" style="color:white">+34 930005723</a></li>
                            <li><a href="mailto:info@jordimagana.com" style="color:white">info@jordimagana.com</a></li>
                        </ul>
                        <ul class="social-icon-five">
                            <li class="facebook"><a href="#"><span class="fa fa-facebook-f"></span></a></li>
                            <li class="twitter"><a href="#"><span class="fa fa-twitter"></span></a></li>
                            <li class="instagram"><a href="#"><span class="fa fa-instagram"></span></a></li>
                        </ul>
                    </div>
                </div>
                
                <!--Form Column-->
                <div class="form-column col-lg-9 col-md-8 col-sm-12 col-xs-12" id="contacto">
                	<div class="inner-column">
                    	<h2>Contacta'ns</h2>
                        <!-- Contact Form -->
                        <div class="contact-form">
                            <!--Comment Form-->
                            <?php 
                                echo @$_SESSION['msj'];
                                unset($_SESSION['msj']);
                            ?>
                            <form method="post" action="paginas/frontend/contacto" onsubmit="return sendForm(this,'#resp')">
                                <div class="row clearfix">
                                    
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group">
                                        <textarea name="message" placeholder="Missatge"></textarea>
                                    </div>
                                    
                                    <div class="col-md-4 col-sm-6 col-xs-12 form-group">
                                        <input type="text" name="nombre" placeholder="Nom i Cognom" required>
                                    </div>
                                    
                                    <div class="col-md-4 col-sm-6 col-xs-12 form-group">
                                        <input type="email" name="email" placeholder="E-mail" required>
                                    </div>
                                    
                                    <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                        <input type="text" name="telefono" placeholder="Telèfon" required>
                                    </div>

                                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                        <label for=""><input type="checkbox" name="politicas" value="1" required="">  He llegit i accepto la <a href="<?= base_url() ?>politica-privacitat.html">política de privacitat</a>* </label>
                                    </div>

                                    <div id="resp"></div>
                                    
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group">
                                        <button class="theme-btn btn-style-one" type="submit">Enviar</button>
                                    </div>
                                    
                                </div>
                            </form>
                                
                        </div>
                        <!--End Contact Form -->
                    </div>
                </div>
                
            </div>
        </div>
    </section>
    <!--End Contact Section-->
    [footer]