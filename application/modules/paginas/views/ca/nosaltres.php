[menu]
<!--Page Title-->
<section class="page-title">
	<div class="icon-one"></div>
	<div class="icon-two"></div>
	<div class="auto-container">
		<div class="icon-three"></div>
		<div class="icon-six"></div>
		<div class="icon-five"></div>
		<div class="icon-four"></div>
		<div class="icon-ten"></div>
		<h2>Nosalt<span class="white-color">res</span> <span class="blue-circle"></span></h2>
	</div>
</section>
<!--End Page Title-->

<!--About Section-->
<section class="about-section">
	
	<div class="row clearfix" style="margin-left: 0; margin-right: 0;">
		<!--Column-->
		<div class="auto-container">
			<div class="column col-md-6 col-sm-12 col-xs-12">
				<div class="row clearfix">
					
					<!--About Block-->
					<div class="about-block style-two col-md-6 col-sm-6 col-xs-12">
						<div class="inner-box">
							<div class="icon-box">
								<img src="http://www.jordimagana.com/theme/theme/images/4.svg" style=" width: 22%;vertical-align: top;" alt="">
							</div>
							<h3><a href="<?= base_url() ?>contacta.html">Disseny</a></h3>
							<div class="block-text">Disseny corporatiu, publicitari, editorial, packaging, multimèdia, il·lustacions...... </div>
						</div>
					</div>
					
					<!--About Block-->
					<div class="about-block style-two col-md-6 col-sm-6 col-xs-12">
						<div class="inner-box">
							<div class="icon-box">
								<img src="http://www.jordimagana.com/theme/theme/images/5.svg" style=" width: 22%;vertical-align: top;" alt="">
							</div>
							<h3><a href="<?= base_url() ?>contacta.html">Desenvolupament Web</a></h3>
							<div class="block-text">Webs Corportiva, botiges online, jocs, redisseny de web, App's, posicionament SEO... </div>
						</div>
					</div>
					
					<!--About Block-->
					<div class="about-block style-two col-md-6 col-sm-6 col-xs-12">
						<div class="inner-box">
							<div class="icon-box">
								<img src="http://www.jordimagana.com/theme/theme/images/6.svg" style=" width: 22%;vertical-align: top;" alt="">
							</div>
							<h3><a href="<?= base_url() ?>contacta.html">Comunicació</a></h3>
							<div class="block-text">Continguts per a webs, catàlegs, notes de premsa, blogs..., gestió de xarxes socials, esdeveniments i rodes de premsa.</div>
						</div>
					</div>
					
					<!--About Block-->
					<div class="about-block style-two col-md-6 col-sm-6 col-xs-12">
						<div class="inner-box">
							<div class="icon-box">
								<img src="http://www.jordimagana.com/theme/theme/images/7.svg" style=" width: 22%;vertical-align: top;" alt="">
							</div>
							<h3><a href="<?= base_url() ?>contacta.html">Vull començar un projecte</a></h3>
							<div class="block-text">
								Explica'ns quin és el projecte que tens en ment i t'ajudarem.
							</div>
							<div class="contact-form" style="margin:20px 0">
								<a href="<?= base_url() ?>contacta.html#formulario">
									<button class="theme-btn btn-style-one" type="button">Solicitar</button>
								</a>
							</div>
						</div>
					</div>
					
				</div>
			</div>
		</div>
		<!--Column-->
		<div class="video-column col-md-6 col-sm-12 col-xs-12" style="padding:0">
			<div class="inner-column" style="padding:0">
				
				<!--Video Box-->
				<div class="video-box">
					<figure class="image">
						<img src="[base_url]theme/theme/images/resource/video-img.jpg" alt="">
					</figure>
					
				</div>
				
			</div>
		</div>
		
	</div>
	
</section>
<!--End About Section-->

<!--Mission Section-->
<section class="about-block">
	<div class="row clearfix" style="margin-left: -15px; margin-right: 0">
		<div class="auto-container">
			<!--Column-->
			<div class="col-md-6 col-sm-12 col-xs-12" style="margin-bottom: 30px;">
				
					<h2>L'equip de Jordi Magaña</h2>
					<div class="text" style="line-height: 2.5em">Som un equip multidisciplinari i trilingüe en els sectors del disseny gràfic, la comunicació i el desenvolupament de webs i apps. Comptem amb més de 30 anys d’experiència desenvolupant projectes d'èxit.  Disposem d’una gran xarxa de col·laboradors especialitzats en àmbits molt concrets del camp de la comunicació que ens permeten garantir qualitat i professionalitat.</div>		
				
			</div>
		
			<!--Skill Column-->
			<div class="skill-column col-md-6 col-sm-12 col-xs-12">
				
				<div class="inner-column">
					<h2>Les nostres habilitats</h2>
					
					<div class="progress-bars">
						
						<!--Skill Item-->
						<div class="bar-item">
							<div class="skill-header clearfix">
								<div class="skill-title">Disseny</div>
							</div>
							<div class="skill-bar">
								<div class="bar-inner"><div class="bar progress-line" data-width="100"><div class="skill-percentage"><div class="count-box"><span class="count-text" data-speed="2000" data-stop="100">0</span></div></div></div></div>
							</div>
						</div>
						<!--Skill Item-->
						<div class="bar-item">
							<div class="skill-header clearfix">
								<div class="skill-title">Web</div>
							</div>
							<div class="skill-bar">
								<div class="bar-inner"><div class="bar progress-line" data-width="100"><div class="skill-percentage"><div class="count-box"><span class="count-text" data-speed="2000" data-stop="100">0</span></div></div></div></div>
							</div>
						</div>
						<!--Skill Item-->
						<div class="bar-item">
							<div class="skill-header clearfix">
								<div class="skill-title">Editorial</div>
							</div>
							<div class="skill-bar">
								<div class="bar-inner"><div class="bar progress-line" data-width="95"><div class="skill-percentage"><div class="count-box"><span class="count-text" data-speed="2000" data-stop="95">0</span></div></div></div></div>
							</div>
						</div>
						<!--Skill Item-->
						<div class="bar-item">
							<div class="skill-header clearfix">
								<div class="skill-title">Comunicació</div>
							</div>
							<div class="skill-bar">
								<div class="bar-inner"><div class="bar progress-line" data-width="94"><div class="skill-percentage"><div class="count-box"><span class="count-text" data-speed="2000" data-stop="94">0</span></div></div></div></div>
							</div>
						</div>
					</div>
					
				</div>
			</div>			
		</div>
	</div>
</section>
<!--End Mission Section-->

<!--Team Section-->
<section class="team-section">
	<div class="icon-three"></div>
	<div class="auto-container">
		<div class="icon-one"></div>
		<div class="icon-two"></div>
		<div class="sec-title centered">
			<div class="title">Equip</div>
			<h2>Professionals al teu servei</h2>
		</div>
		<div class="row clearfix">
			
			<!--Team Block-->
			<div class="team-block col-md-3 col-sm-6 col-xs-6">
				<div class="inner-box">
					<div class="image">
						<img src="[base_url]theme/theme/images/resource/team-1.jpg" alt="" />
						<div class="social-box">
							<a href="#"><span class="fa fa-facebook"></span></a>
							<a href="#"><span class="fa fa-twitter"></span></a>
							<a href="#"><span class="fa fa-pinterest-p"></span></a>
						</div>
					</div>
					<div class="lower-box">
						<h3><a href="#">Jordi Magaña</a></h3>
						<div class="designation">Direcció i disseny</div>
					</div>
				</div>
			</div>
			<!--End Team Block-->
			
			<!--Team Block-->
			<div class="team-block col-md-3 col-sm-6 col-xs-6">
				<div class="inner-box">
					<div class="image">
						<img src="[base_url]theme/theme/images/resource/team-2.jpg" alt="" />
						<div class="social-box">
							<a href="#"><span class="fa fa-facebook"></span></a>
							<a href="#"><span class="fa fa-twitter"></span></a>
							<a href="#"><span class="fa fa-pinterest-p"></span></a>
						</div>
					</div>
					<div class="lower-box">
						<h3><a href="#">Eliana Tusell</a></h3>
						<div class="designation">Continguts i comunicació</div>
					</div>
				</div>
			</div>
			<!--End Team Block-->
			
			<!--Team Block-->
			<div class="team-block col-md-3 col-sm-6 col-xs-6">
				<div class="inner-box">
					<div class="image">
						<img src="[base_url]theme/theme/images/resource/team-3.jpg" alt="" />
						<div class="social-box">
							<a href="#"><span class="fa fa-facebook"></span></a>
							<a href="#"><span class="fa fa-twitter"></span></a>
							<a href="#"><span class="fa fa-pinterest-p"></span></a>
						</div>
					</div>
					<div class="lower-box">
						<h3><a href="#">Míriam Salazar</a></h3>
						<div class="designation">Comunicació i periodisme</div>
					</div>
				</div>
			</div>
			<!--End Team Block-->
			
			<!--Team Block-->
			<div class="team-block col-md-3 col-sm-6 col-xs-6">
				<div class="inner-box">
					<div class="image">
						<img src="[base_url]theme/theme/images/resource/team-4.jpg" alt="" />
						<div class="social-box">
							<a href="#"><span class="fa fa-facebook"></span></a>
							<a href="#"><span class="fa fa-twitter"></span></a>
							<a href="#"><span class="fa fa-pinterest-p"></span></a>
						</div>
					</div>
					<div class="lower-box">
						<h3><a href="#">Jonathan Cardozo</a></h3>
						<div class="designation">Programació</div>
					</div>
				</div>
			</div>
			<!--End Team Block-->
			
		</div>
	</div>
</section>
<!--End Team Section-->

<!--Testimonial Section-->
<section class="testimonial-section">

	<div class="icon-one"></div>
	<div class="auto-container">
		<div class="icon-two"></div>
		<div class="single-item-carousel owl-carousel owl-theme">
		
		
			
			<!--Testimonial Block-->
			<!-- 
<div class="testimonial-block">
				<div class="inner-box">
					<div class="quote-icon">
						<span class="icon flaticon-straight-quotes"></span>
					</div>
					<div class="text">Efficiently unleash cross-media information without cross-media value. Quickly maximize timely deliverables for real-time schemas. Dramatically maintain clicks-and-mortar solutions without functional solutions.</div>
					<div class="designation">Nattasha <span>- Ui designer</span></div>
				</div>
			</div>
 -->
			
			<!--Testimonial Block-->
			<!-- 
<div class="testimonial-block">
				<div class="inner-box">
					<div class="quote-icon">
						<span class="icon flaticon-straight-quotes"></span>
					</div>
					<div class="text">Efficiently unleash cross-media information without cross-media value. Quickly maximize timely deliverables for real-time schemas. Dramatically maintain clicks-and-mortar solutions without functional solutions.</div>
					<div class="designation">Nattasha <span>- Ui designer</span></div>
				</div>
			</div>
 -->
			
			<!--Testimonial Block-->
			<!-- 
<div class="testimonial-block">
				<div class="inner-box">
					<div class="quote-icon">
						<span class="icon flaticon-straight-quotes"></span>
					</div>
					<div class="text">Efficiently unleash cross-media information without cross-media value. Quickly maximize timely deliverables for real-time schemas. Dramatically maintain clicks-and-mortar solutions without functional solutions.</div>
					<div class="designation">Nattasha <span>- Ui designer</span></div>
				</div>
			</div>
 -->
			
		</div>
		
	</div>
</section>
<!--End Testimonial Section-->

<section class="clients-section">
<div class="icon-three"></div>
	<div class="auto-container">
		<div class="icon-one"></div>
		<div class="icon-two"></div>
		<div class="sec-title centered">
			<div class="title">Clients</div>
			<h2>Per a qui treballem</h2>
		</div>
	<div class="auto-container">
		<div class="inner-container">
			<div class="clearfix">
				<div class="column col-md-3 col-sm-6 col-xs-6">
					<div class="image">
						<a href="#"><img src="[base_url]theme/theme/images/clients/1.png" alt="" /></a>
					</div>
				</div>
				<div class="column col-md-3 col-sm-6 col-xs-6">
					<div class="image">
						<a href="#"><img src="[base_url]theme/theme/images/clients/2.png" alt="" /></a>
					</div>
				</div>
				<div class="column col-md-3 col-sm-6 col-xs-6">
					<div class="image">
						<a href="#"><img src="[base_url]theme/theme/images/clients/3.png" alt="" /></a>
					</div>
				</div>
				<div class="column col-md-3 col-sm-6 col-xs-6">
					<div class="image">
						<a href="#"><img src="[base_url]theme/theme/images/clients/4.png" alt="" /></a>
					</div>
				</div>
				<div class="column col-md-3 col-sm-6 col-xs-6">
					<div class="image">
						<a href="#"><img src="[base_url]theme/theme/images/clients/5.png" alt="" /></a>
					</div>
				</div>
				<div class="column col-md-3 col-sm-6 col-xs-6">
					<div class="image">
						<a href="#"><img src="[base_url]theme/theme/images/clients/6.png" alt="" /></a>
					</div>
				</div>
				<div class="column col-md-3 col-sm-6 col-xs-6">
					<div class="image">
						<a href="#"><img src="[base_url]theme/theme/images/clients/7.png" alt="" /></a>
					</div>
				</div>
				<div class="column col-md-3 col-sm-6 col-xs-6">
					<div class="image">
						<a href="#"><img src="[base_url]theme/theme/images/clients/8.png" alt="" /></a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>


<section class="testimonial-section">

</section>


<!--Full Width Gallery Section-->
    <section class="fullwidth-gallery-section">
    	<div class="icon-three"></div>
		<div class="auto-container">
			<div class="icon-one"></div>
			<div class="icon-two"></div>
			<div class="sec-title centered">
				<div class="title">La nostra oficina</div>
				<h2>A qui treballem</h2>
			</div>
		</div>
    	<div class="outer-container">
        	<div class="clearfix">
            	
                <!--Gallery Block-->
                <div class="gallery-item col-lg-3 col-md-4 col-sm-6 col-xs-12">
                    <div class="inner-box wow fadeIn" data-wow-delay="0ms" data-wow-duration="1500ms">
                        <div class="image">
                            <img src="<?= base_url() ?>theme/theme/images/gallery/36.jpg" alt="" />
                            <div class="overlay-box">
                                <div class="overlay-inner">
                                    <div class="content">
                                        <a href="<?= base_url() ?>theme/theme/images/gallery/36.jpg" data-fancybox="gallery" class="lightbox-image plus" title="Image Title Here"><span class="flaticon-signs"></span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!--Gallery Block-->
                <div class="gallery-item col-lg-3 col-md-4 col-sm-6 col-xs-12">
                    <div class="inner-box wow fadeIn" data-wow-delay="500ms" data-wow-duration="1500ms">
                        <div class="image">
                            <img src="<?= base_url() ?>theme/theme/images/gallery/37.jpg" alt="" />
                            <div class="overlay-box">
                                <div class="overlay-inner">
                                    <div class="content">
                                        <a href="<?= base_url() ?>theme/theme/images/gallery/37.jpg" data-fancybox="gallery" class="lightbox-image plus" title="Image Title Here"><span class="flaticon-signs"></span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!--Gallery Block-->
                <div class="gallery-item col-lg-3 col-md-4 col-sm-6 col-xs-12">
                    <div class="inner-box wow fadeIn" data-wow-delay="1000ms" data-wow-duration="1500ms">
                        <div class="image">
                            <img src="<?= base_url() ?>theme/theme/images/gallery/38.jpg" alt="" />
                            <div class="overlay-box">
                                <div class="overlay-inner">
                                    <div class="content">
                                        <a href="<?= base_url() ?>theme/theme/images/gallery/38.jpg" data-fancybox="gallery" class="lightbox-image plus" title="Image Title Here"><span class="flaticon-signs"></span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!--Gallery Block-->
                <div class="gallery-item col-lg-3 col-md-4 col-sm-6 col-xs-12">
                    <div class="inner-box wow fadeIn" data-wow-delay="1500ms" data-wow-duration="1500ms">
                        <div class="image">
                            <img src="<?= base_url() ?>theme/theme/images/gallery/39.jpg" alt="" />
                            <div class="overlay-box">
                                <div class="overlay-inner">
                                    <div class="content">
                                        <a href="<?= base_url() ?>theme/theme/images/gallery/39.jpg" data-fancybox="gallery" class="lightbox-image plus" title="Image Title Here"><span class="flaticon-signs"></span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!--Gallery Block-->
                <div class="gallery-item col-lg-3 col-md-4 col-sm-6 col-xs-12">
                    <div class="inner-box wow fadeIn" data-wow-delay="0ms" data-wow-duration="1500ms">
                        <div class="image">
                            <img src="<?= base_url() ?>theme/theme/images/gallery/40.jpg" alt="" />
                            <div class="overlay-box">
                                <div class="overlay-inner">
                                    <div class="content">
                                        <a href="<?= base_url() ?>theme/theme/images/gallery/40.jpg" data-fancybox="gallery" class="lightbox-image plus" title="Image Title Here"><span class="flaticon-signs"></span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!--Gallery Block-->
                <div class="gallery-item col-lg-3 col-md-4 col-sm-6 col-xs-12">
                    <div class="inner-box wow fadeIn" data-wow-delay="500ms" data-wow-duration="1500ms">
                        <div class="image">
                            <img src="<?= base_url() ?>theme/theme/images/gallery/41.jpg" alt="" />
                            <div class="overlay-box">
                                <div class="overlay-inner">
                                    <div class="content">
                                        <a href="<?= base_url() ?>theme/theme/images/gallery/41.jpg" data-fancybox="gallery" class="lightbox-image plus" title="Image Title Here"><span class="flaticon-signs"></span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!--Gallery Block-->
                <div class="gallery-item col-lg-3 col-md-4 col-sm-6 col-xs-12">
                    <div class="inner-box wow fadeIn" data-wow-delay="1000ms" data-wow-duration="1500ms">
                        <div class="image">
                            <img src="<?= base_url() ?>theme/theme/images/gallery/42.jpg" alt="" />
                            <div class="overlay-box">
                                <div class="overlay-inner">
                                    <div class="content">
                                        <a href="<?= base_url() ?>theme/theme/images/gallery/42.jpg" data-fancybox="gallery" class="lightbox-image plus" title="Image Title Here"><span class="flaticon-signs"></span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!--Gallery Block-->
                <div class="gallery-item col-lg-3 col-md-4 col-sm-6 col-xs-12">
                    <div class="inner-box wow fadeIn" data-wow-delay="1500ms" data-wow-duration="1500ms">
                        <div class="image">
                            <img src="<?= base_url() ?>theme/theme/images/gallery/43.jpg" alt="" />
                            <div class="overlay-box">
                                <div class="overlay-inner">
                                    <div class="content">
                                        <a href="<?= base_url() ?>theme/theme/images/gallery/43.jpg" data-fancybox="gallery" class="lightbox-image plus" title="Image Title Here"><span class="flaticon-signs"></span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!--Gallery Block-->
                <div class="gallery-item col-lg-3 col-md-4 col-sm-6 col-xs-12">
                    <div class="inner-box wow fadeIn" data-wow-delay="500ms" data-wow-duration="1500ms">
                        <div class="image">
                            <img src="<?= base_url() ?>theme/theme/images/gallery/44.jpg" alt="" />
                            <div class="overlay-box">
                                <div class="overlay-inner">
                                    <div class="content">
                                        <a href="<?= base_url() ?>theme/theme/images/gallery/44.jpg" data-fancybox="gallery" class="lightbox-image plus" title="Image Title Here"><span class="flaticon-signs"></span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!--Gallery Block-->
                <div class="gallery-item col-lg-3 col-md-4 col-sm-6 col-xs-12">
                    <div class="inner-box wow fadeIn" data-wow-delay="1000ms" data-wow-duration="1500ms">
                        <div class="image">
                            <img src="<?= base_url() ?>theme/theme/images/gallery/45.jpg" alt="" />
                            <div class="overlay-box">
                                <div class="overlay-inner">
                                    <div class="content">
                                        <a href="<?= base_url() ?>theme/theme/images/gallery/45.jpg" data-fancybox="gallery" class="lightbox-image plus" title="Image Title Here"><span class="flaticon-signs"></span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!--Gallery Block-->
                <div class="gallery-item col-lg-3 col-md-4 col-sm-6 col-xs-12">
                    <div class="inner-box wow fadeIn" data-wow-delay="1500ms" data-wow-duration="1500ms">
                        <div class="image">
                            <img src="<?= base_url() ?>theme/theme/images/gallery/46.jpg" alt="" />
                            <div class="overlay-box">
                                <div class="overlay-inner">
                                    <div class="content">
                                        <a href="<?= base_url() ?>theme/theme/images/gallery/46.jpg" data-fancybox="gallery" class="lightbox-image plus" title="Image Title Here"><span class="flaticon-signs"></span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!--Gallery Block-->
                <div class="gallery-item col-lg-3 col-md-4 col-sm-6 col-xs-12">
                    <div class="inner-box wow fadeIn" data-wow-delay="1500ms" data-wow-duration="1500ms">
                        <div class="image">
                            <img src="<?= base_url() ?>theme/theme/images/gallery/47.jpg" alt="" />
                            <div class="overlay-box">
                                <div class="overlay-inner">
                                    <div class="content">
                                        <a href="<?= base_url() ?>theme/theme/images/gallery/47.jpg" data-fancybox="gallery" class="lightbox-image plus" title="Image Title Here"><span class="flaticon-signs"></span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </section>
[footer]