[menu]
<!--Page Title-->
    <section class="page-title">
    	<div class="icon-one"></div>
        <div class="icon-two"></div>
		<div class="auto-container">
        	<div class="icon-four"></div>
            <div class="icon-five"></div>
			<div class="icon-six"></div>
        	<div class="icon-three"></div>
            <div class="icon-eight"></div>
        	<h2>Noticies <span class="gradiant-circle"></span></h2>
        </div>
    </section>
    <!--End Page Title-->
    
    <!--Blog Masonry Section-->
    <div class="blog-masonry-section">
    	<div class="auto-container">
        	
            <div class="row clearfix">
					
                [foreach:blog]
                    <!--News Block Two-->
                    <div class="news-block-two style-two col-md-4 col-sm-6 col-xs-12">
                        <div class="inner-box">
                            <div class="image">
                                <a href="[link]">
                                    <img src="[foto]" alt="" />
                                </a>
                            </div>
                            <div class="lower-content">
                                <h2><a href="[link]">[titulo]</a></h2>
                                <ul class="post-meta">
                                    <li><a href="[link]">[fecha]</a></li>
                                    <!--<li><a href="[link]">Like <span>12</span>  /</a></li>
                                    <li><a href="[link]">Comment <span>25</span></a></li>-->
                                </ul>
                                <div class="text">[texto]</div>
                                <a href="[link]" class="read-more">Veure més <span class="arrow flaticon-arrow-pointing-to-right"></span></a>
                            </div>
                        </div>
                    </div>
                [/foreach]
                
                
            </div>
            
        </div>
    </div>
    <!--End Blog Masonry Section-->
    [footer]