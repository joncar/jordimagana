<?php       
	$response = empty($_SESSION['msj'])?'':$_SESSION['msj'];
	$page = str_replace('[response]',$response,$page); 
	unset($_SESSION['msj']);

	$response = empty($_SESSION['msj2'])?'':$_SESSION['msj2'];
	$page = str_replace('[msg2]',$response,$page); 
	unset($_SESSION['msj']); 

	//Reemplace			
	$page = str_replace('[menu]',$this->load->view($this->theme.'menu',array(),TRUE),$page);		
	$page = str_replace('[footer]',$this->load->view($this->theme.'footer',array(),TRUE),$page);	
	$page = str_replace('[base_url]',base_url(),$page);		
	if($this->user->log){
		$usuarioItem = '<li class="menu-item-has-children"><a href="#">YO</a><ul  class="submenu"><li><a href="'.base_url('panel').'">Cuenta</a></li><li><a href="'.base_url('main/unlog').'">Salir</a></li></ul></li>';
	}else{
		$usuarioItem = '<li><a href="'.base_url('panel').'">Cuenta</a></li>';
	}	 
	$this->db->limit(3);
	$this->db->order_by('fecha','DESC');
	$this->db->where('blog_categorias_id',4);
	$this->db->where('idioma',$_SESSION['lang']);
	$blog = $this->db->get_where('blog',array('status'=>1));
	foreach($blog->result() as $nn=>$b){
		$b->link = site_url('blog/'.toURL($b->id.'-'.$b->titulo));
        $b->foto = base_url('img/blog/'.$b->foto);
        $b->fecha = strftime('%B %d, %Y',strtotime($b->fecha));
        $b->texto = strip_tags($b->texto);
        $b->texto = cortar_palabras($b->texto,28).'...';
        $b->comentarios = $this->db->get_where('comentarios',array('blog_id'=>$b->id))->num_rows();            
        $b->descripcion = cortar_palabras(strip_tags($b->texto),20);
        $b->posicion = $nn==0?'left':'';
        $b->posicion = $nn==1?'center':$b->posicion;
        $b->posicion = $nn==2?'right':$b->posicion;
	}

	$page = $this->querys->fillFields($page,array('blog'=>$blog));
	$page = str_replace('<span style="display: none;"></span>','',$page);
    echo $page;
?>