

        <!--<div class="spacer-small"></div>-->
             
         <div class="fullwidth-section align-center" style="background:#f5f6f7;">
            <div class="fullwidth-content wrapper">
                
                <h3><span>Subscríbete a nuestra Newsletter</span></h3>
                <h5 class="subtitle-1">Sé el primero en saber las novedades de MIF</h5>
                <div class="spacer-mini"></div>
                <form action="#" method="post" id="newsletter-widget-form" onsubmit="return subscribir(this)">
                	<div id="result2"></div>                	
                    <input type="email" id="newsletter-email" name="email" placeholder="E-mail">                     
                    <input type="submit" id="newsletter-submit" value="Enviar">
                    <div class="form-row">
                        <input type="checkbox" name="politicas" value="1"> Acepto las <a href="<?= base_url('politicas-de-privacidad.html') ?>" rel="canonical" target="_new">política de privacidad</a>
                    </div>
                </form>
            
            </div>
        </div>
<!-- FOOTER -->  
	    <footer id="footer" class="footer-custom text-light" style="background:url([base_url]theme/theme/files/uploads/hero-portfolio-1.jpg) center bottom; background-size: cover;">
	       	<div class="footer-inner wrapper">
	       
	            <div class="column-section clearfix">
	            	<div class="column two-fifth">
	                	<div class="widget">
	                    	<img src="[base_url]theme/theme/files/uploads/logo-sudo-light.png" alt="Logo">
	                        <div class="spacer-mini"></div>
	                        <p>Vive una experiencia única, vive tu verano con tus amigos, conoce gente nueva, disfruta, remójate, baila, ríe, canta, juega, salta.... Así es Mallorca Island Festival, una experiencia que no te puedes perder, con los mejores dj's y en una isla mágica.</p>
	                    </div>
	                    
	                     <div class="widget">
	                        <ul class="socialmedia-widget hover-fade-1">
	                            <li class="facebook"><a href="https://facebook.com/mallorcaislandfestival" target="_new"></a></li>	                            
	                            <li class="youtube"><a href="https://www.youtube.com/channel/UCIp1ihBnekNPNIbNQ_9PG4A " target="_new"></a></li>
	                            <li class="soundcloud"><a href="https://www.soundcloud.com/finalia" target="_new"></a></li>
	                            <li class="instagram"><a href="https://www.instagram.com/mallorca_island_festival/" target="_new"></a></li>
	                        </ul>
	                    </div>
	                    
	                </div>
	            	<div class="column one-fifth">
	                	<div class="widget">
	                    	<h6 class="widget-title uppercase">Nosotros</h6>
	                        <ul class="menu">
	                        	<li><a rel="canonical" href="[base_url]grupo-finalia.html">Grupo Finalia</a></li>
	                        	<li><a rel="canonical" href="[base_url]trabaja-con-nosotros">Trabaja con nosotros</a></li>
	                        	<li><a rel="canonical" href="[base_url]galeria">Galeria</a></li>
	                        	<li><a rel="canonical" href="[base_url]blog">Experiencias MIF</a></li>
	                        	<li><a rel="canonical" href="[base_url]contacto.html">Contacta</a></li>
	                        </ul>
	                    </div>
	                </div>
	            	<div class="column one-fifth">
	                	<div class="widget">
	                    	<h6 class="widget-title uppercase">El viaje</h6>
	                        <ul class="menu">
	                        	<li><a rel="canonical" href="[base_url]excursiones.html">Excursiones</a></li>
	                        	<li><a rel="canonical" href="[base_url]actividades.html">Actividades</a></li>
	                        	<li><a rel="canonical" href="[base_url]discotecas.html">Discotecas</a></li>
	                        	<li><a rel="canonical" href="[base_url]como-reservar.html">¿Cómo reservar?</a></li>
	                        	<li><a rel="canonical" href="[base_url]store">Reserva</a></li>
	                        </ul>
	                    </div>
	                </div>
	            	<div class="column one-fifth last-col">
	                	<div class="widget widget_recent_entries">	
	                        <h6 class="widget-title uppercase">Experiencias MIF</h6>
	                        <ul>
	                            [foreach:blog_footer]
	                            <li>
	                            	<img src="[foto]" style="display: inline-block; width: 45%;">
	                            	<div style="display: inline-block;width: 50%; margin-left: 3%; position: relative;">
		                            	<a rel="canonical" href="[link]" style="display: block;height: 5em;overflow: hidden;">[titulo]</a>		                            	
		                            	<span class="post-date">[fecha]</span>
	                            	</div>
	                            </li>
	                            [/foreach]
	                        </ul>
	                    </div>
	                </div>
	            </div>
	                                    
	            <a id="backtotop" href="#">Back to top</a>
	        </div>
	        
	         <div class="copyright">
	         	<small>Copyright &copy; <?= date("Y") ?> MIF | 
	         		<a href="[base_url]aviso-legal.html">Aviso legal</a> | 
	         		<a href="[base_url]politicas-de-privacidad.html">Política de privacidad</a> | 
	         		<a href="[base_url]sitemap">Sitemap</a> | 
	         		<a href="[base_url]condiciones-generales.html">Condiciones Generales</a> |
	         		- Hecho por <a href="http://www.jordimagaña.com">Jordi Magaña</a></small>
	         </div>
	    </footer>