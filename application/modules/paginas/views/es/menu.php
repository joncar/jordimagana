<div class="header-inner clearfix">
            
            <!-- LOGO -->
            <div id="logo" class="left-float">
                <a href="[base_url]">
                    <img id="scroll-logo" src="[base_url]theme/theme/files/uploads/logo-hd.png"  alt="Logo Scroll">
                    <img id="dark-logo" src="[base_url]theme/theme/files/uploads/logo-hd.png"  alt="Logo Dark">
                    <img id="light-logo" src="[base_url]theme/theme/files/uploads/logo-hd.png"  alt="Logo Light">
                </a>
            </div>
            
            <!-- MAIN NAVIGATION -->
            <div id="menu" class="right-float">
<a href="#" class="responsive-nav-toggle"><span class="hamburger"></span></a>
                <div class="menu-inner">
                    <nav id="main-nav">
                        <ul>
                            <li><a href="[base_url]">INICIO</a></li>
                            <li class="menu-item-has-children">
                                <a href="#">NOSOTROS</a>
                                <ul class="submenu">
                                    <li><a rel="canonical" href="[base_url]grupo-finalia.html">GRUPO FINALIA</a></li>
                                    <li><a rel="canonical" href="[base_url]instalaciones.html">INSTALACIONES</a></li>
                                    <li><a rel="canonical" href="[base_url]nuestro-equipo.html">NUESTRO EQUIPO</a></li>
                                    <li><a rel="canonical" href="[base_url]trabaja-con-nosotros">TRABAJA CON NOSOTROS</a></li>                                    
                                </ul>
                            </li>
                            <li class="menu-item-has-children">
                                <a href="#">VIAJE</a>
                                <ul class="submenu"><!-- vertical box -->
                                    <li><a rel="canonical" href="[base_url]que-es-mif.html">¿QUÉ ES MIF?</a></li>
                                    <li><a rel="canonical" href="[base_url]que-incluye.html">¿QUÉ INCLUYE?</a></li>
                                    <li><a rel="canonical" href="[base_url]el-resort.html">EL RESORT</a></li>
                                    <li><a rel="canonical" href="[base_url]excursiones.html">EXCURSIONES</a></li>
                                    <li><a rel="canonical" href="[base_url]actividades.html">ACTIVIDADES</a></li>
                                    <li><a rel="canonical" href="[base_url]discotecas.html">DISCOTECAS</a></li>
                                    <li><a rel="canonical" href="[base_url]como-reservar.html">¿CÓMO RESERVAR?</a></li>
                                    <li><a rel="canonical" href="[base_url]zona-padres.html">ZONA PADRES</a></li>
                                    <li><a rel="canonical" href="[base_url]faqs.html">FAQ's</a></li>
                                    <li><a rel="canonical" href="[base_url]feed-back.html">FEED BACK</a></li>
                                </ul>
                            </li>
                            <li><a rel="canonical" href="[base_url]discotecas.html#influencers">INFLUENCERS</a></li>
                            <li class="menu-item-has-children">
                                <a rel="canonical" href="#">GALERIA</a>
                                <ul class="submenu">
                                    <li><a href="[base_url]galeria">Galeria</a></li>
                                    <li><a href="[base_url]aftermovie.html">Aftermovie 2018</a></li>
                                </ul>
                            </li>
                            <li class="menuItemResaltado"><a rel="canonical" href="[base_url]store">RESERVA ONLINE</a></li>
                            <li class="menu-item-has-children">
                                <a rel="canonical" href="#">CONTACTA</a>
                                <ul class="submenu">
                                    <li><a href="[base_url]contacto.html">Contacto</a></li>
                                    <li><a href="[base_url]contacto-patrocinador.html">Contacto patrocinador</a></li>
                                </ul>
                            </li>
                            [menuUser]
                        </ul>
                    </nav>
                    
                    <div id="menu-misc" class="clearfix">    
                        
                        <!-- HEADER SEARCH -->
                        <div id="header-search">
                            <a href="#" id="show-search"><i class="fa fa-search"></i></a>
                            <div class="header-search-content">
                                <form action="#" method="get">
                                    <a href="#" id="close-search"></a>
                                    <input type="text" name="q" class="form-control" value="" placeholder="Buscar">
                                    <h5 class="subtitle-1">... & y pulsa comenzar</h5>
                                </form>
                                <div class="search-outer"></div>
                            </div>
                        </div>
                        
                        <!-- HEADER CART -->
                        <div id="header-cart" class="menubar-cart">
                            [carritoNav]
                        </div><!-- END #header-cart -->
                        
                        <!-- HEADER LANGUAGE -->
                        <!--<div id="header-language">
                            <a href="#" id="show-language">En <i class="ion ion-ios-arrow-down"></i></a>
                            <div class="header-language-content">
                                <ul class="lang-select">
                                    <li><a href="#">De</a></li>
                                    <li><a href="#">Fr</a></li>
                                </ul>
                            </div>
                        </div><!-- END #header-language -->
                        
                    </div><!-- END #menu-misc -->
                </div><!-- END .menu-inner -->
                </div><!-- END #menu -->
                <div id="header-cart" class="menubar-cart menubar-cart-responsive">
                    [carritoNav]
                </div><!-- END #header-cart -->
        </div> <!-- END .header-inner -->