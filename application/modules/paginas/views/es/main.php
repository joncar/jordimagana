<!-- HEADER -->
	<header id="header" class="header-transparent transparent-light sub-dark">        		
    	[menu]
	</header>
    
    <!-- HERO  -->
	<section id="hero" class="hero-full text-light videobg-section"
					   	data-videotype="html5" 					   	
					   	data-videoratio="16/9"
					   	data-videoloop="true"
					   	data-videomute="true"
					   	data-videoposter="[base_url]theme/theme/files/uploads/neon-lights-poster.jpg"
					   	data-videooverlaycolor="#000000"
					   	data-videooverlayopacity="0.4"
                        data-videomp4="[base_url]videos/video.mp4"
                        data-videoogg="[base_url]videos/video.ogg"
                        data-videoogv="[base_url]videos/video.ogv"
                        data-videowebm="[base_url]videos/video.WebM"
                        >
    	
        <div id="page-title" class="wrapper align-center">
            <!--<h3 class="subtitle-2">Youtube Video Background with Sound</h3>-->
            <h1 class="">Vive la <br/>experiencia</h1>
            <h3 class="subtitle-1">Únete a la experiencia</h3>
        </div> <!-- END #page-title -->
        <a href="#" id="scroll-down"></a>
    </section>
    <!-- HERO -->
    <section id="page-body" class="notoppadding">
        <div>
            <div class="column-section boxed-sticky clearfix">
                <div class="column one-third nopadding">
                    <a rel="canonical" href="[catalogo]" class="thumb-overlay overlay-effect-2 text-light" target="_new">
                        <img src="[base_url]theme/theme/files/uploads/catalogo.jpg" alt="Catálogo on-line de MIF">
                        <img src="[base_url]theme/theme/files/uploads/catalogoonline.jpg" class="hover-pic" alt="Catálogo on-line de MIF">
                        <div class="overlay-caption">
                            <h4 class="caption-name portfolio-name uppercase textshadow"><span class="main3botones">Catálogo</span> <br/> online</h4>
                        </div>
                    </a>                    
                </div>
                <div class="column one-third nopadding">
                    <a rel="canonical" href="[base_url]mas-informacion" class="thumb-overlay overlay-effect-2 text-light">
                        <img src="[base_url]theme/theme/files/uploads/organiza.jpg" alt="Organiza tu viaje">
                        <img src="[base_url]theme/theme/files/uploads/organizatuviaje.jpg" class="hover-pic" alt="Organiza tu viaje">
                        <div class="overlay-caption">
                            <!--<h6 class="caption-sub portfolio-category subtitle-2">with</h6>
                            <hr class="small fat">-->
                            <h4 class="caption-name portfolio-name uppercase textshadow"><span class="main3botones">Organiza</span> <br/>tu viaje</h4>
                        </div>
                    </a>   
                </div>
                <div class="column one-third last-col nopadding">
                    <a rel="canonical" href="https://mallorcaislandfestival.com/tienda" class="thumb-overlay overlay-effect-2 text-light">
                        <img src="[base_url]theme/theme/files/uploads/merchandising.jpg" alt="Merchandising Online">
                        <img src="[base_url]theme/theme/files/uploads/merchandisingonline.jpg" class="hover-pic" alt="Merchandising Online">
                        <div class="overlay-caption">
                            <h4 class="caption-name portfolio-name uppercase textshadow"><span class="main3botones">Merchandising</span> <br/> Online</h4>
                        </div>
                    </a>   
                </div>
            </div>
        </div>              
        <div class="fullwidth-section" style="background:#f5f6f7;">
            <div class="fullwidth-content wrapper">
            
                <div class="column-section clearfix">
                    <div class="column two-fifth">
                        <h3 class="uppercase colored">BIENVENIDO A MALLORCA ISLAND FESTIVAL</h3>
                        <hr class="small small fat">
                    </div>
                    <div class="column two-fifth">
                        <p>Vive una experiencia única, vive tu verano con tus amigos, conoce gente nueva, disfruta, remójate, baila, ríe, canta, juega, salta.... Así es Mallorca Island Festival, una experiencia que no te puedes perder, con los mejores dj's y en una isla mágica.</p>
                    </div>
                    <div class="column one-fifth last-col">
                        <div class="sr-counter counter-big">
                            <span class="counter-value">+</span> 
                            <span class="counter-value">40000</span>
                        </div>
                        <h4 class="uppercase">participantes y subiendo!</h4>
                    </div>
                </div>
                
            </div> <!-- END .wrapper -->
        </div>       
             
        <div class="fullwidth-section text-light videobg-section"
                        data-videotype="youtube" 
                        data-videoyoutubeid="DGYATjL5NwU"
                        data-videoratio="16/9"
                        data-videoloop="true"                        
                        data-videoposter="<?= base_url() ?>theme/theme/files/uploads/900x600-dark.jpg"
                        data-videooverlaycolor="#000000"
                        data-videooverlayopacity="0.7"
                        >
            <div class="fullwidth-content wrapper-small align-center">
                
                <h3 class="concurso">CONCURSO MIF</h3>
                <hr class="small fat colored align-center">
                <h4 class="subtitle-1 align-center">¿QUIERES GANAR UN VIAJE PARA MIF 2019?</h4>
                <div class="spacer-medium"></div>
                <p>
                    <a rel="canonical" class="sr-button button-4 circled button-icon-text button-medium" href="<?= base_url('concurso') ?>">
                        <i class="fa fa-play"></i> Ir al concurso
                    </a>
                </p>
                
            </div>
        </div>

        <div class="spacer-big"></div>
        <div class="fullwidth-content wrapper">
            <center><h3 class="align-center">Redes sociales</h3>
            <hr class="small fat colored"></center>
            <div class="spacer-medium"></div>

            <ul id="portfolio-filter" class="filter" data-related-grid="portfolio-grid">
                <li class="active">
                    <a href="#" data-filter="*" id="allSocial">
                        <i class="fa fa-globe fa-2x"></i> <br/>
                        Todas
                    </a>
                </li>
                <li>
                    <a data-filter=".instagram" href="#" title="Feed Instagram">
                        <i class="fa fa-2x fa-instagram"></i><br>
                        Instagram
                    </a>
                </li>
                <li>
                    <a data-filter=".facebook" href="#" title="Feed Facebook">
                        <i class="fa fa-2x fa-facebook-square"></i><br/>
                        Facebook
                    </a>
                </li>
                <li>
                    <a data-filter=".youtube" href="#" title="Feed Youtube">
                        <i class="fa fa-2x fa-youtube-square"></i><br/>
                        Youtube
                    </a>
                </li>
            </ul>
        
            <div id="portfolio-grid" class="social-feed-container isotope-grid gallery-container style-column-4 isotope-spaced clearfix"  uk-lightbox>
                
                
            </div>
        </div>
        <div class="spacer-big"></div>        
        <div class="fullwidth-section" style="background:#f5f6f7;">
            <div class="fullwidth-content wrapper">
        
                <h3 class="align-center">Experiencias MIF</h3>
                <center><hr class="small fat colored"></center>
                <div class="spacer-medium"></div>
            
                <div id="blog-grid" class="isotope-grid blog-container style-column-3 fitrows isotope-spaced">
                    [foreach:blog]
                        <div class="isotope-item blog-item">
                            <div class="blog-media">
                                <a rel="canonical" href="[link]" class="thumb-overlay">
                                    <img src="[foto]" alt="[titulo]">
                                    <img src="[foto]" class="hover-pic" alt="[titulo]">
                                </a>
                            </div>
                            
                            <div class="blog-desc">
                                <div class="blog-headline">
                                    <h6 class="post-category uppercase align-center">[user]</h6>
                                    <h5 class="post-name"><a rel="canonical" href="[link]"><strong>[titulo]</strong></a></h5>
                                </div>
                                <p>[texto]</p>
                                <ul class="blog-meta">
                                    <li class="post-date">[fecha]</li>                            
                                </ul>
                            </div>
                        </div>
                    [/foreach]
                </div> <!-- END #blog-grid -->
                
                <p class="align-center"><a rel="canonical" href="[base_url]blog" class="sr-button">Ir al blog</a></p>
        
            </div>
        </div> <!-- END .fullwidth-section -->

        
        <div class="fullwidth-section text-light notoppadding nobottompadding" style="background: #1a1a1a;">
                <div class="fullwidth-content">
                
                    <div class="column-section clearfix bordered-sticky align-center">
                        <div class="column one-fifth"><img src="[base_url]theme/theme/files/uploads/logo-spotify-light.png" class="has-animation animated"></div>
                        <div class="column one-fifth"><img src="[base_url]theme/theme/files/uploads/logo-fwa-light.png"  class="has-animation animated" data-delay="200"></div>
                        <div class="column one-fifth"><img src="[base_url]theme/theme/files/uploads/logo-google-light.png" class="has-animation animated" data-delay="400"></div>
                        <div class="column one-fifth"><img src="[base_url]theme/theme/files/uploads/logo-wordpress-light.png"  class="has-animation animated" data-delay="600"></div>
                        <div class="column one-fifth last-col"><img src="[base_url]theme/theme/files/uploads/logo-yt-light.png" class="has-animation animated" data-delay="800"></div>
                    </div> <!-- END .column-section -->
                    
                    <div class="column-section clearfix bordered-sticky align-center">
                        <div class="column one-fifth"><img src="[base_url]theme/theme/files/uploads/logo-sass-light.png" class="has-animation animated"></div>
                        <div class="column one-fifth"><img src="[base_url]theme/theme/files/uploads/logo-ableton-light.png" class="has-animation animated" data-delay="200"></div>
                        <div class="column one-fifth"><img src="[base_url]theme/theme/files/uploads/logo-ni-light.png" class="has-animation animated" data-delay="400"></div>
                        <div class="column one-fifth"><img src="[base_url]theme/theme/files/uploads/logo-spotify-light2.png" class="has-animation animated" data-delay="600"></div>
                        <div class="column one-fifth last-col"><img src="[base_url]theme/theme/files/uploads/logo-google-light2.png" class="has-animation animated" data-delay="800"></div>
                    </div>

                    <div class="column-section clearfix bordered-sticky align-center">
                        <div class="column one-fifth"><img src="[base_url]theme/theme/files/uploads/logo-google-light3.png" class="has-animation animated"></div>
                        <div class="column one-fifth"><img src="[base_url]theme/theme/files/uploads/logo-google-light4.png" class="has-animation animated" data-delay="200"></div>
                        <div class="column one-fifth"><img src="[base_url]theme/theme/files/uploads/logo-google-light5.png" class="has-animation animated" data-delay="400"></div>
                        <div class="column one-fifth"><img src="[base_url]theme/theme/files/uploads/logo-google-light6.png" class="has-animation animated" data-delay="600"></div>
                        <div class="column one-fifth last-col"><img src="[base_url]theme/theme/files/uploads/logo-google-light7.png" class="has-animation animated" data-delay="800"></div>
                    </div> <!-- END .column-section -->
                    
                </div>
            </div>


        <div>[footer]</div>

    </section>
    <!-- PAGEBODY -->
    